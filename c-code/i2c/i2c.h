#ifndef I2C_H
#define I2C_H


#define I2C_FS_PATH "/dev/i2c-1"

class i2c
{
    public:
        i2c();
        i2c(const char* bus,int address);
        ~i2c();

        void write2i2c(char* buf, int transferbytes);
        char* read2i2c(char* buf, int transferbytes);

    private:
        int addr;
        int fd;
};
#endif // I2C_H
