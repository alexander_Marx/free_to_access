#include "i2c.h"
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

i2c::i2c()
{
    //ctor
    addr = 0x20;

    // Open I2C Bus
    fd = open(I2C_FS_PATH, O_RDWR);
    if(fd < 0) printf("i2c failed to open\n");
    // Set Addr
    if(ioctl(fd, I2C_SLAVE, addr) < 0)
		printf("failed to acquire bus\n");
}
i2c::i2c(const char* bus,int address)
{
	//ctor
	    addr = address;

	    // Open I2C Bus
	    fd = open(bus, O_RDWR);
	    if(fd < 0) printf("i2c failed to open\n");
	    // Set Addr
	    if(ioctl(fd, I2C_SLAVE, addr) < 0)
			printf("failed to acquire bus\n");
}

i2c::~i2c()
{
    //dtor
    char buf[1];
    buf[0]=0x0;

    write2i2c(buf,1);
    close(fd);
}

void i2c::write2i2c(char* buf, int transferbytes)
{
/*
	S     (1 bit) : Start bit
	P     (1 bit) : Stop bit
	Rd/Wr (1 bit) : Read/Write bit. Rd equals 1, Wr equals 0.
	A, NA (1 bit) : Accept and reverse accept bit.
	Addr  (7 bits): I2C 7 bit address. Note that this can be expanded as usual to
	                get a 10 bit I2C address.
	Comm  (8 bits): Command byte, a data byte which often selects a register on
	                the device.
	Data  (8 bits): A plain data byte. Sometimes, I write DataLow, DataHigh
	                for 16 bit data.
	Count (8 bits): A data byte containing the length of a block operation.

	[..]: Data sent by I2C device, as opposed to data sent by the host adapter.


	Simple send transaction
	======================

	This corresponds to i2c_master_send.

	  S Addr Wr [A] Data [A] Data [A] ... [A] Data [A] P

	  1 0100 0000 1 DDDD DDDD 1 1

	  1 0x40
*/
	if( write(fd,buf,transferbytes) != (transferbytes) )
	{
		printf("i2c write failed\n");
		close(fd);
		exit(EXIT_FAILURE);
	}
}
char* i2c::read2i2c(char* buf, int transferbytes)
{
/*	Key to symbols
	==============

	S     (1 bit) : Start bit
	P     (1 bit) : Stop bit
	Rd/Wr (1 bit) : Read/Write bit. Rd equals 1, Wr equals 0.
	A, NA (1 bit) : Accept and reverse accept bit.
	Addr  (7 bits): I2C 7 bit address. Note that this can be expanded as usual to
	                get a 10 bit I2C address.
	Comm  (8 bits): Command byte, a data byte which often selects a register on
	                the device.
	Data  (8 bits): A plain data byte. Sometimes, I write DataLow, DataHigh
	                for 16 bit data.
	Count (8 bits): A data byte containing the length of a block operation.

	[..]: Data sent by I2C device, as opposed to data sent by the host adapter.


	Simple receive transaction
	===========================

	This corresponds to i2c_master_recv

	  S Addr Rd [A] [Data] A [Data] A ... A [Data] NA P

*/

	if(read(fd,&buf,transferbytes) != (transferbytes) )
    {
        printf("i2c read failed\n");
    }

	return buf;
}
