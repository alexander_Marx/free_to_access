/*
 * LEDLAUFLEISTE.h
 *
 *  Created on: 21.01.2015
 *      Author: Haeckl.M.0
 */


#ifndef LEDLAUFLEISTE_H_
#define LEDLAUFLEISTE_H_


#include "../i2c/i2c.h"


class LEDLAUFLEISTE {
public:
	LEDLAUFLEISTE(const char* bus, int address);
	~LEDLAUFLEISTE();
	void display(int number);

private:
	i2c* LED;
	void write2led(char* buf, int transferbytes);
	char* read2led(char* buf, int transferbytes);
};

#endif /* LEDLAUFLEISTE_H_ */
