/*
 * LEDLAUFLEISTE.cpp
 *
 *  Created on: 21.01.2015
 *      Author: Haeckl.M.0
 */
#include <stdio.h>
#include "LEDLAUFLEISTE.h"


LEDLAUFLEISTE::LEDLAUFLEISTE(const char* bus, int address) {
	// TODO Auto-generated constructor stub
	LED = new i2c(bus,address);
}

LEDLAUFLEISTE::~LEDLAUFLEISTE() {
	// TODO Auto-generated destructor stub
	delete(LED);
}

void LEDLAUFLEISTE::write2led(char* buf, int transferbytes)
{
/*
	S     (1 bit) : Start bit
	P     (1 bit) : Stop bit
	Rd/Wr (1 bit) : Read/Write bit. Rd equals 1, Wr equals 0.
	A, NA (1 bit) : Accept and reverse accept bit.
	Addr  (7 bits): I2C 7 bit address. Note that this can be expanded as usual to
	                get a 10 bit I2C address.
	Comm  (8 bits): Command byte, a data byte which often selects a register on
	                the device.
	Data  (8 bits): A plain data byte. Sometimes, I write DataLow, DataHigh
	                for 16 bit data.
	Count (8 bits): A data byte containing the length of a block operation.

	[..]: Data sent by I2C device, as opposed to data sent by the host adapter.


	Simple send transaction
	======================

	This corresponds to i2c_master_send.

	  S Addr Wr [A] Data [A] Data [A] ... [A] Data [A] P

	  1 0100 0000 1 DDDD DDDD 1 1

	  1 0x40
*/

    char* buffer=new char[transferbytes+1];
    buffer[0]=0x40;
    for (int i=1;i<=transferbytes;i++)
        buffer[i] = buf[i-1];
    LED->write2i2c(buffer,transferbytes+1);
	delete(buffer);
}

char* LEDLAUFLEISTE::read2led(char* buf, int transferbytes)
{
/*	Key to symbols
	==============

	S     (1 bit) : Start bit
	P     (1 bit) : Stop bit
	Rd/Wr (1 bit) : Read/Write bit. Rd equals 1, Wr equals 0.
	A, NA (1 bit) : Accept and reverse accept bit.
	Addr  (7 bits): I2C 7 bit address. Note that this can be expanded as usual to
	                get a 10 bit I2C address.
	Comm  (8 bits): Command byte, a data byte which often selects a register on
	                the device.
	Data  (8 bits): A plain data byte. Sometimes, I write DataLow, DataHigh
	                for 16 bit data.
	Count (8 bits): A data byte containing the length of a block operation.

	[..]: Data sent by I2C device, as opposed to data sent by the host adapter.


	Simple receive transaction
	===========================

	This corresponds to i2c_master_recv

	  S Addr Rd [A] [Data] A [Data] A ... A [Data] NA P

	 1 0100 0001 1 DDDD DDDD 1 1

	  1 0x41
*/

    char* buffer=new char[transferbytes+1];
    buffer[0]=0x41;
    for (int i=1;i<=transferbytes;i++)
        buffer[i] = buf[i-1];
    delete(buf);

	buf = LED->read2i2c(buffer,transferbytes);

    delete(buffer);
    return buf;
}

// Programm um eine LED Laufleiste zu zeigen.
void LEDLAUFLEISTE::display(int number)
{
    if ( number > 8 || number < 0)
    {
        printf("False Number only beetween 0-8.");
        return;
    }
    char* buf = new char;
    switch(number)
    {
    	case 0:
                    *buf=0b00000000;
                    LED->write2i2c(buf,1);
                    break;
        case 1:
                    *buf=0b00000001;
                    LED->write2i2c(buf,1);
                    break;
        case 2:
                    *buf=0b00000010;
                    LED->write2i2c(buf,1);
                    break;
        case 3:
                    *buf=0b00000100;
                    LED->write2i2c(buf,1);
                    break;
        case 4:
                    *buf=0b00001000;
                    LED->write2i2c(buf,1);
                    break;
        case 5:
                    *buf=0b00010000;
                    LED->write2i2c(buf,1);
                    break;
        case 6:
                    *buf=0b00100000;
                    LED->write2i2c(buf,1);
                    break;
        case 7:
                    *buf=0b01000000;
                    LED->write2i2c(buf,1);
                    break;
        case 8:
                    *buf=0b10000000;
                    LED->write2i2c(buf,1);
                    break;
        default:
                    printf("False Number only beetween 0-7.");
    }
    delete(buf);
}
