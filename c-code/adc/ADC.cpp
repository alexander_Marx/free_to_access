/*
 * ADC.cpp
 *
 *  Created on: 21.01.2015
 *      Author: Haeckl.M.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include "ADC.h"
using namespace std;

ADC::ADC(int channel) {
	// TODO Auto-generated constructor stub

	char* adctempfile="/sys/devices/ocp.3/helper.12/AIN";
	char* number = ConvertInt2Char(channel);
	adcfile = new char[strlen(adctempfile)+strlen(number)];
	strcpy(adcfile,adctempfile);
	strcat(adcfile,number);


	fd = open((const char*)adcfile,O_RDONLY);
	if(fd < 0)
	{
		printf("could not open %s\nLoad Overlay\n",adcfile);
		fd = open(OVERLAY_FILE,O_RDWR);
			if(fd < 0)
			{
				printf("could not open %s\n",OVERLAY_FILE);
				close(fd);
				exit(EXIT_FAILURE);
			}


			int res = write(fd,LOADFIRMWARE,strlen(LOADFIRMWARE));
			if (res != strlen(LOADFIRMWARE) )
			{
				printf("could not Write to %s\n",OVERLAY_FILE);
				close(fd);
				exit(EXIT_FAILURE);
			}
			close(fd);

			printf("try it again to open ADC File\n");
			fd = open((const char*)adcfile,O_RDONLY);
			if(fd < 0)
			{
				printf("could not open %s\n",adcfile);
				close(fd);
				exit(EXIT_FAILURE);
			}
		}
	printf("opened ADC File: %s\n",adcfile);
	close(fd);

}

ADC::~ADC() {
	// TODO Auto-generated destructor stub
	close(fd);
}

void ADC::open_to_ADC()
{
	fd = open((const char*)adcfile,O_RDONLY);
	if(fd < 0)
	{
		printf("could not open %s\n",adcfile);
		close(fd);
		exit(EXIT_FAILURE);
	}
}
void ADC::close_to_ADC()
{
	close(fd);
}

int ADC::read_from_ADC()
{

	int transfer= MAX_BUF;
	char buffer[MAX_BUF];
	memset(buffer,'\0',MAX_BUF);
	open_to_ADC();
		int res = read(fd, &buffer,transfer);
	close_to_ADC();
	return atoi(buffer);
}

char*  ADC::ConvertInt2Char(const int number)
{
	char buffer[MAX_BUF];
	sprintf(buffer,"%d",number);
	return strdup(buffer);
}
