/*
 * ADC.h
 *
 *  Created on: 21.01.2015
 *      Author: Haeckl.M.0
 */

#ifndef ADC_H_
#define ADC_H_

#define LOADFIRMWARE "BB-ADC"
#define OVERLAY_FILE "/sys/devices/bone_capemgr.9/slots"
#define MAX_BUF 64

class ADC {
public:
	ADC(int channel);
	~ADC();
	int read_from_ADC();
private:
	int fd;
	char* adcfile;

	void open_to_ADC();
	void close_to_ADC();
	char* ConvertInt2Char(const int number);

};

#endif /* ADC_H_ */
