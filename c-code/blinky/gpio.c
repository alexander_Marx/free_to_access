#include "gpio.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
/*
 * gpio.c
 *
 *  Created on: 15.01.2015
 *      Author: Haeckl.M.0
 */

//init all
void init(GPIOs* dev)
{
	// FileHandler
    dev->pin[0].FileHandler =  dev->pin[1].FileHandler =  dev->pin[2].FileHandler = 0;

    dev->pin[0].File_Base = (char*) malloc (MAX_BUF);
    dev->pin[1].File_Base = (char*) malloc (MAX_BUF);
	dev->pin[2].File_Base = (char*) malloc (MAX_BUF);

    //PIN1
    //SetFilePath( &dev->pin[0]);
	GPIOunexport(dev->pin[0].pin_number);
    GPIOexport( dev->pin[0].pin_number);
    SetPortDirection( &dev->pin[0],"out");
    OpenPortValue( &dev->pin[0]);

    //PIN2
    //SetFilePath( &dev->pin[1]);
    GPIOunexport(dev->pin[1].pin_number);
    GPIOexport( dev->pin[1].pin_number);
	SetPortDirection( &dev->pin[1],"out");
	OpenPortValue( &dev->pin[1]);

	//PIN3
	//SetFilePath( &dev->pin[2]);
	GPIOunexport(dev->pin[2].pin_number);
	GPIOexport( dev->pin[2].pin_number);
	SetPortDirection( &dev->pin[2],"out");
	OpenPortValue( &dev->pin[2]);
}
void printfFinished()
{
	printf("......finished\n");
}
char*  ConvertInt2Char(const int number)
{
	char buffer[MAX_BUF];
	sprintf(buffer,"%d",number);
	return strdup(buffer);
}
void SetFilePath(PINstruct* pin)
{
/*
	printf("SetFilepath of PIN: %i", pin->pin_number);
	// PATH
	char* path = "/sys/class/gpio/";
	// Convert Int 2 Char
	char* tmp = ConvertInt2Char(pin->pin_number);
	// Build File_Base Path /sys/class/gpio/gpio<number
	strncpy(pin->File_Base,path,strlen(path));
	strncat(pin->File_Base,"gpio",strlen("gpio"));
	strncat(pin->File_Base, tmp,strlen(tmp));
	printf("\n tmp=%s File_Base=%s\n",tmp,pin->File_Base);
	// Free Convert Space
	free (tmp);

	// Set File Path of direction and value also to the File Base
//	printf("File_Value=%s File_Dir=%s File_Base=%s\n",pin->File_Value,pin->File_Dir,pin->File_Base);
//	strncpy(pin->File_Value,pin->File_Base,strlen(pin->File_Base));
//	printf("File_Value=%s File_Dir=%s File_Base=%s\n",pin->File_Value,pin->File_Dir,pin->File_Base);
//	strncpy(pin->File_Dir,pin->File_Base,strlen(pin->File_Base));
//	printf("File_Value=%s File_Dir=%s File_Base=%s\n",pin->File_Value,pin->File_Dir,pin->File_Base);
//	exit(1);
//	// Set Specified File Path
//	strncat(pin->File_Dir,"/direction",strlen("/direction"));
//	printf("File_Value=%s File_Dir=%s File_Base=%s\n",pin->File_Value,pin->File_Dir,pin->File_Base);
//	strncat(pin->File_Value,"/value",strlen("/value"));
//	printf("File_Value=%s File_Dir=%s File_Base=%s\n",pin->File_Value,pin->File_Dir,pin->File_Base);
//
//
//
	printfFinished();
	printf("File_Base: %s\n",pin->File_Base);
//	printf("File_Dir: %s\n",pin->File_Dir);
//	printf("File_Value: %s\n",pin->File_Value);
*/

}
void SetPortDirection(PINstruct* pin, const char* dir)
{
	printf("SetPortDirection to %s on PIN %i",dir,pin->pin_number);
//	printf("File_Dir: %s\n",pin->File_Dir);
//	exit (1);
	char buf[MAX_BUF];
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "%d/direction",pin->pin_number);
	int FH = open (buf, O_RDWR);
	if (FH < 0 )
	{
		printf("Could not open %s file\n",buf);
		exit(1);
	}
	if ( write(FH,dir,strlen(dir)) != strlen(dir) )
		printf("Could not write to %s",buf);
	close(FH);
	printfFinished();
}
void OpenPortValue(PINstruct* pin)
{
	printf("OpenPortValue on Port: %i",pin->pin_number);
	char buf[MAX_BUF];
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "%d/value",pin->pin_number);
	pin->FileHandler = open (buf, O_RDWR);
	if (pin->FileHandler < 0 )
		        {
				                printf("Could not open %s file\n",buf);
						                exit(1);
								        }
	printfFinished();
}
void GPIOexport(const int pin_number)
{
    printf("\nExport GPIO number %i",pin_number);
    int FH=0;
    char str[MAX_BUF];
    char* file = "/sys/class/gpio/export";
    FH = open(file, O_WRONLY);
    if (  FH < 0 )
        {
            printf("Could not open Export file![%s]\n",file);
            exit(EXIT_FAILURE);
        }
    int len = snprintf(str, sizeof(str), "%d", pin_number);
    int ret = write(FH,str,len);
    if ( ret < 0 )
        printf("Could not write to export file! no export from %s\n",str);

    close(FH);

    printfFinished();
}
void GPIOunexport(const int pin_number)
{
    printf("Unexport GPIO number %i",pin_number);
	int FH=0;
	char str[MAX_BUF];
	char* file = "/sys/class/gpio/unexport";
	FH = open(file, O_WRONLY);
	if (  FH < 0 )
		{
			printf("Could not open Unexport file![%s]\n",file);
			exit(EXIT_FAILURE);
		}
	int len = snprintf(str, sizeof(str), "%d", pin_number);
	int ret = write(FH,str,len);
	if ( ret < 0 )
		printf("Could not write to unexport file! no export from %s\n",str);
	close(FH);

	printfFinished();
}

