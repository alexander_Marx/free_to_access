/*
 * gpio.h
 *
 *  Created on: 15.01.2015
 *      Author: Haeckl.M.0
 */

#ifndef BLINKY_GPIO_H_
#define BLINKY_GPIO_H_

// for Char Variables to get all the same length.
#define MAX_BUF 64
#define SYSFS_GPIO_DIR "/sys/class/gpio/gpio"

// Struct for PIN
typedef struct PIN
{
    int FileHandler;
    int pin_number;
    char* File_Base;
    char* File_Dir;
    char* File_Value;
}PINstruct;
// Struct for LED
typedef struct GPIO
{
    PINstruct* pin;
}GPIOs;

void init(GPIOs* dev);
void SetFilePath(PINstruct* pin);
void SetPortDirection(PINstruct* pin, const char* dir);
void OpenPortValue(PINstruct* pin);
void GPIOexport(const int pin_number);
void GPIOunexport(const int pin_number);
void printfFinished();
char* ConvertInt2Char(const int number);

#endif /* BLINKY_GPIO_H_ */
