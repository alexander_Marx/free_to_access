#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include "gpio.h"


/*

PINs:

    P9.12       =   gpio60
    P8.14       =   gpio26
    P8.20       =   gpio27

*/

#define PIN1 60
#define PIN2 26
#define PIN3 27


//Functions
void change_color(const PINstruct* pin,const char val);
//void blinky_WRAPPER(const GPIOs* led, const char* values);
void blinky_WRAPPER(const GPIOs* led, int number);
void blinky(const GPIOs* led);




int main()
{
	printf("initialize....\n");
	//create a gpio object
	GPIOs* led = (GPIOs*) malloc (1);
	// create into the gpio object 3 pins
	led->pin = (PINstruct*) malloc (3);
	// PIN Number
	led->pin[0].pin_number = PIN1;
	led->pin[1].pin_number = PIN2;
	led->pin[2].pin_number = PIN3;
    init(led);
    printf("initialize....finished\n");


    blinky(led);
    return 0;
}



void change_color(const PINstruct* pin,const char val)
{
    char tmp[2]; tmp[1]='\0'; tmp[0]=val;
    if ( write( pin->FileHandler,tmp,2 ) != 2 )
    	printf("write less bytes than it should by change_color of PINNumber: %i and val %s",pin->pin_number,val);
}
/*void blinky_WRAPPER(const GPIOs* led, const char* values)
{
	printf("%c %c %c\n",values[0],values[1],values[2]);
	change_color(&led->pin[0],values[2]);
	change_color(&led->pin[1],values[1]);
	change_color(&led->pin[2],values[0]);
	sleep(1);
}*/
void blinky_WRAPPER(const GPIOs* led, int number)
{
	printf("NUMBER=%i\n",number);
	if (number > 4)
	{
		change_color(&led->pin[2],'1');
		number-=4;
	}
	else
		change_color(&led->pin[2],'0');

	if (number > 2)
	{
		change_color(&led->pin[1],'1');
		number-=2;
	}
	else
		change_color(&led->pin[1],'0');


	if (number == 1)
		change_color(&led->pin[0],'1');
	else
		change_color(&led->pin[0],'0');

	sleep(1);
}
// For blinked LED
void blinky(const GPIOs* led)
{
    int i = 2;
    int count=0b111;
    while(count > 0)
    {
    	// 0 0 0
/*        blinky_WRAPPER(led,"000");
        // 0 0 1
        blinky_WRAPPER(led,"001");
        // 0 1 0
        blinky_WRAPPER(led,"010");
        // 0 1 1
        blinky_WRAPPER(led,"011");
        // 1 0 0
        blinky_WRAPPER(led,"100");
        // 1 0 1
        blinky_WRAPPER(led,"101");
        // 1 1 0
        blinky_WRAPPER(led,"110");
        // 1 1 1
        blinky_WRAPPER(led,"111");*/

    	blinky_WRAPPER(led,count--);
    }

    blinky_WRAPPER(led,0b111);
    close(led->pin[0].FileHandler);
    close(led->pin[1].FileHandler);
    close(led->pin[2].FileHandler);
}
