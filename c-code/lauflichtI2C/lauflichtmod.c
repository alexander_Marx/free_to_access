#define _POSIX_C_SOURCE 200809
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>

int main(){

  int file,i;
  char buf[3]={0};

  struct timespec wait;

  wait.tv_sec=0;
  wait.tv_nsec=500000000;

  // I2C address
  int addr = 0x20;

  if ((file = open("/dev/i2c-1", O_RDWR)) < 0) {
    printf("open error! %d\n",file);
    return 1;
  }

  if (ioctl(file, I2C_SLAVE, addr) < 0) {
    printf("address error!\n");
    return 1;
  }
while (1){
  buf[0] = 0x7f;
  for (i=0;i<8;i++) {
    write(file, buf, 1);
    nanosleep(&wait,NULL);
    buf[0] = ~buf[0];
    buf[0]>>=1;
    buf[0] = ~buf[0];
  }

  buf[0] = 0xFE; 
  for (i=0;i<6;i++) {
    buf[0] = ~buf[0];
    buf[0]<<=1;
    buf[0] = ~buf[0];
    write(file, buf, 1);
    nanosleep(&wait,NULL);
  }
}

  close(file);
  return 0;
}
