/*
 * main.cpp
 *
 *  Created on: 21.01.2015
 *      Author: Haeckl.M.0
 *      Weblink: http://beaglebone.cameon.net/home/reading-the-analog-inputs-adc
 */
#include <iostream>
#include <stdio.h>
#include "../ledlaufleiste/LEDLAUFLEISTE.h"
#include "../adc/ADC.h"

int main()
{
	LEDLAUFLEISTE led("/dev/i2c-2",0x20);
	ADC adc5(5);

	while (true)
	{
		printf("Value of ADC5 = %i\n",adc5.read_from_ADC());
		int act = adc5.read_from_ADC();
		if (act == 0)
			led.display(0);
		if(act > 0 && act <= 150)
			led.display(1);
		if(act > 150 && act <= 300)
			led.display(2);
		if(act > 300 && act <= 450)
			led.display(3);
		if(act > 450 && act <= 600)
			led.display(4);
		if(act > 600 && act <= 750)
			led.display(5);
		if(act > 750 && act <= 900)
			led.display(6);
		if(act > 900 && act <= 1050)
			led.display(7);
		if(act > 1050 && act <= 1200)
			led.display(8);
		sleep(1);
	}

	return 0;
}
