/*
 * SPI.h
 *
 *  Created on: 21.01.2015
 *      Author: Haeckl.M.0
 */

#ifndef SPI_H_
#define SPI_H_

// SPIDEV "/dev/spidev1.0"

class SPI {
public:
	SPI(const char* bus,int address);
	~SPI();
    void write2spi(char* buf, int transferbytes);
    char* read2spi(char* buf, int transferbytes);

private:
	int addr;
	int fd;

	int getBitsperWord();
	void setBitsperWord(int Bits);
};

#endif /* SPI_H_ */
