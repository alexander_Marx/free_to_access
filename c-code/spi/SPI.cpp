/*
 * SPI.cpp
 *
 *  Created on: 21.01.2015
 *      Author: Haeckl.M.0
 */

#include "SPI.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <linux/spi/spidev.h>
#include <sys/ioctl.h>
#include <linux/types.h>

SPI::SPI(const char* bus,int address)
{
	// TODO Auto-generated constructor stub
	addr = address;

	fd = open(bus, O_SYNC | O_RDWR);
	if(fd < 0)
	{
		printf("spi failed to open\n");
		exit(EXIT_FAILURE);
	}
}

SPI::~SPI()
{
	// TODO Auto-generated destructor stub
	close(fd);
}

void SPI::write2spi(char* buf, int transferbytes)
{
	//do any
	int res = write(fd,buf,transferbytes);
	if (res < transferbytes)
		printf("Could not all Bytes write only %i bytes write\n",res);
}
char* SPI::read2spi(char* buf, int transferbytes)
{
	//do any
	int res = read(fd,buf,transferbytes);
	return strdup(buf);
}

int SPI::getBitsperWord()
{
	//do any
	uint8_t bpw;
	int ret = ioctl (fd, SPI_IOC_RD_BITS_PER_WORD, &bpw);
	if (ret < 0)
		printf("failed reading bits per word\n");
	switch (bpw)
	    {
	    case 8:
	      printf("read bits per word as 8\n");
	      return 8;
	    case 16:
	      printf("read bits per word as 16\n");
	      return 16;
	    default:
	      printf("bits per word not recognised\n");
	      return -1;
	    }
}
void SPI::setBitsperWord(int Bits)
{
	//do any
	if (Bits != 8 || Bits != 16)
	{
		printf("False Bitcount only 8 or 16 you get %i\n",Bits);
	}
	int ret = ioctl (fd, SPI_IOC_WR_BITS_PER_WORD, &Bits);

	if (ret == -1)
	{
	  print("failed setting bits per word\n");
	  exit(EXIT_FAILURE);
	}
}
